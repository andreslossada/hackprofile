class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :lastname
      t.string :subtitle
      t.string :email
      t.string :user_name
      t.text :bio
      t.date :birthdate
      t.binary :photo
      # t.references hobby:, foreign_key: true


      t.timestamps
    end
  end
end
