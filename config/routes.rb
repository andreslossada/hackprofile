Rails.application.routes.draw do
  get 'certifications/index'
  get 'home/index'
  resources :hobbies
  resources :users
  root 'home#index'
  resources :certifications
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
