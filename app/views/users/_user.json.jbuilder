json.extract! user, :id, :name, :lastname, :subtitle, :email, :user_name, :bio, :birthdate, :photo, :created_at, :updated_at
json.url user_url(user, format: :json)
