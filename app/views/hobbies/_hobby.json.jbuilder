json.extract! hobby, :id, :name, :description, :preference, :category, :created_at, :updated_at
json.url hobby_url(hobby, format: :json)
