class User < ApplicationRecord
    has_many :hobbies
    has_many :certifications
    has_many :friends
    validates_uniqueness_of :email, :user_name
    has_one_attached :profile_pic
    before_create :image_empty
    accepts_nested_attributes_for :hobbies, :certifications, reject_if: :all_blank

    private

    def image_empty
        unless profile_pic.attached?
            self.profile_pic.attach(io: File.open(Rails.root.join("app/assets/images/default.jpg")), filename: 'default.jpg', content_type:"image/png")
        end
    end

end
