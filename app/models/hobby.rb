class Hobby < ApplicationRecord
    belongs_to :user
    enum category: [:Musica, :Lectoescritura, :Aventura, :Fitness, :Coleccionismo, :Entretenimiento, :Construccion, :Gastronomia ]

end
