class CertificationsController < ApplicationController
  # before_action :set_certification, only: [:show, :destroy]

  def index
    @certifications = Certification.all
  end
 # GET /hobbies/new
 def new
  @certification = Certification.new
end

# GET /hobbies/1/edit
def edit
end

# POST /hobbies
# POST /hobbies.json
def create
  @certification = Certification.new(certification_params)

  respond_to do |format|
    if @certification.save
      format.html { redirect_to @certification, notice: 'cert was successfully created.' }
      format.json { render :show, status: :created, location: @certification }
    else
      format.html { render :new }
      format.json { render json: @certification.errors, status: :unprocessable_entity }
    end
  end
end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @certification = Certification.find_by(id: params[:id])
    @certification.destroy
    respond_to do |format|
      format.html { redirect_to certifications_url, notice: 'cert  was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_certification
    @certification = certification.find(params[:id])
  end

  def certification_params
    params.require(:certification).permit(:name, :url, :user_id)
  end

end
